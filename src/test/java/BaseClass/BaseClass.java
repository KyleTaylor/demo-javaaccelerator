package BaseClass;

import java_accelerator.*;
import java_accelerator.AlmMetaData;
import java_accelerator.TestClass;
import java_accelerator.TestAssertion;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import almconnector.*;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
public class BaseClass {


    //--------------------------------------------------------------------------------------//
    //----------------------------------TESTNG ANNOTATIONS----------------------------------//
    //--------------------------------------------------------------------------------------//

    @BeforeSuite
    public void beforeSuite() {
        SuiteSetup();
        //TODO implement the Helper.loadObjectRepository() method and trigger it from here
        //TODO the method should check if objectRepository.json exists, then load it into Helper.objectRepository, using Jackson's ObjectMapper
    }

    @AfterSuite
    public void afterSuite() { Helper.saveObjectRepository(); }

    @BeforeMethod
    public void beforeMethod(Method m) {
        //Makes sure user has created correct metadata for test method & skips test if incorrect
        if (usingAlm())
            CheckAlmMethodDataForErrors(m);
    }

    @AfterMethod
    public void afterMethod(ITestResult result) throws IOException{
        //Saves screenshots as zip files
        DisposeDriver();

        PostResults(result, usingAlm());
    }

    //--------------------------------------------------------------------------------------//
    //----------------------------------------DRIVER----------------------------------------//
    //--------------------------------------------------------------------------------------//

    public BaseClass() {
        //Creates location to properties file
        Config.LoadProperties();
        //Populate on runtime
        TestSeverity.PopulateFromConfigProperties();
    }

    private void SuiteSetup() {
        //Deletes screenshots from last test
        WebSiteControl.deleteScreenshots(Config.prop.getProperty("ScreenshotLocation"));
    }

    private String remoteDriverUserName = Config.prop.getProperty("RemoteDriverUserName");
    private String remoteDriverPassword = Config.prop.getProperty("RemoteDriverPassword");
    private String remoteDriverUrl = Config.prop.getProperty("RemoteDriverURL");
    private WebdriverContext _context;
    private WebDriver Driver;
    public WebdriverContext webdriver;
    private void setWebdriverContext() {
        boolean isNewBrowser;
        //Stores exception
        try {
            isNewBrowser = this._context != null || this._context.getDriver() != this.Driver;
        }catch (Exception e){
            isNewBrowser = true;
        }
        if (isNewBrowser){
            this._context = new WebdriverContext(this.Driver);
            this._context.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
        webdriver = this._context;
    }

    /**
     * Starts up a browser session.
     * @param browser Name of browser, must be fetched from Constants class.
     */
    protected void StartDriver (String browser){
        //Defaults to local driver
        CreateMyDriver(browser, Integer.parseInt(Config.prop.getProperty("IMPLICIT_WAIT_DEFAULT")));
    }
    /**
     * Starts up a browser session.
     * @param browser Name of browser, must be fetched from Constants class.
     * @param usingRemoteDriver Option to use remote driver for test. Defaults to false
     */
    protected void StartDriver (String browser, boolean usingRemoteDriver){
        UsingRemoteDriver(usingRemoteDriver);
        CreateMyDriver(browser, Integer.parseInt(Config.prop.getProperty("IMPLICIT_WAIT_DEFAULT")));
    }
    /**
     * Starts up a browser session.
     * @param browser Name of browser, must be fetched from Constants class.
     * @param implicitWaitSec Optional timeout for spinning up driver
     */
    protected void StartDriver(String browser, int implicitWaitSec){
        //Defaults to local driver
        CreateMyDriver(browser, implicitWaitSec);
    }
    /**
     * Starts up a browser session.
     * @param browser Name of browser, must be fetched from Constants class.
     * @param usingRemoteDriver Option to use remote driver for test. Defaults to false
     * @param implicitWaitSec Optional timeout for spinning up driver
     */
    protected void StartDriver (String browser, boolean usingRemoteDriver, int implicitWaitSec){
        UsingRemoteDriver(usingRemoteDriver);
        CreateMyDriver(browser, implicitWaitSec);
    }

    private boolean usingRemoteDriver = false;
    private void UsingRemoteDriver(boolean usingRemoteDriver){
        this.usingRemoteDriver = usingRemoteDriver;
    }

    private void CreateMyDriver(String browserType, int implicitWaitSec)
    {
            if (Driver == null) {
                DesiredCapabilities caps = new DesiredCapabilities();

                //---------------CHROME----------------
                if (browserType.equals(Config.prop.getProperty("CHROME"))) {
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.addArguments("no-sandbox");
                    chromeOptions.setCapability("useAutomationExtension", false);

                    //------------REMOTE CHROME------------
                    if (usingRemoteDriver) {
                        caps = GenerateBrowserCapabilities(browserType);
                        //CreateRemoteDriver
                        try {
                            Driver = new RemoteWebDriver(new URL(remoteDriverUrl), caps);
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                    } else {
                        System.setProperty("webdriver.chrome.driver", FetchDriver("chromedriver.exe"));
                        Driver = new ChromeDriver();
                    }
                    setWebdriverContext();
                }

                //---------------FIREFOX----------------
                else if (browserType.equals(Config.prop.getProperty("FIREFOX"))) {
                    FirefoxOptions fxProfile = new FirefoxOptions();
                    fxProfile.addPreference("network.proxy.type", 4);
                    fxProfile.addPreference("browser.download.folderList", 2);
                    fxProfile.addPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.ms-excel");

                    //------------REMOTE FIREFOX------------
                    if (usingRemoteDriver) {
                        caps = GenerateBrowserCapabilities(browserType);
                        //CreateRemoteDriver
                        try {
                            Driver = new RemoteWebDriver(new URL(remoteDriverUrl), caps);
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                    } else {
                        System.setProperty("webdriver.gecko.driver", FetchDriver("geckodriver.exe"));
                        Driver = new FirefoxDriver();
                    }
                    setWebdriverContext();
                }
                //---------------INTERNET EXPLORER----------------
                else if (browserType.equals(Config.prop.getProperty("IE"))) {
                    InternetExplorerOptions options = new InternetExplorerOptions();
                    options.ignoreZoomSettings();
                    options.introduceFlakinessByIgnoringSecurityDomains();
                    options.usePerProcessProxy();
                    caps = GenerateBrowserCapabilities(browserType);

                    //------------REMOTE INTERNET EXPLORER------------
                    if (usingRemoteDriver) {
                        //CreateRemoteDriver
                        try {
                            Driver = new RemoteWebDriver(new URL(remoteDriverUrl), caps);
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                    } else {
                        System.setProperty("webdriver.ie.driver", FetchDriver("IEDriverServer.exe"));
                        Driver = new InternetExplorerDriver();
                    }
                    setWebdriverContext();
                }

                //---------------REMOTE MOBILE (BROWSERSTACK)----------------
                else if (browserType.equals(Config.prop.getProperty("Mobile"))) {
                    Map<String, Object> browserstackOptions = new HashMap<>();
                    browserstackOptions.put("osVersion", "11");
                    browserstackOptions.put("deviceName", "iPhone 8 Plus");
                    browserstackOptions.put("realMobile", "false");
                    browserstackOptions.put("local", "true");
                    browserstackOptions.put("userName", remoteDriverUserName);
                    browserstackOptions.put("accessKey", remoteDriverPassword);
                    caps.setCapability("bstack:options", browserstackOptions);

                    //CreateRemoteDriver
                    try {
                        Driver = new RemoteWebDriver(new URL(remoteDriverUrl), caps);
                    } catch (Exception e) {
                        System.out.println("Can not create Mobile driver. Exception: " + e);
                        Driver = null;
                    }
                    setWebdriverContext();
                }
            }
            else {
                Driver.quit();
                Driver = null;
                StartDriver(browserType);
            }
        Driver.manage().timeouts().implicitlyWait(implicitWaitSec, TimeUnit.SECONDS);
    }

    //Set Browser & Browser version for driver Capabilities. Chrome, FF, IE
    private DesiredCapabilities GenerateBrowserCapabilities(String browserType){
        DesiredCapabilities caps = new DesiredCapabilities();
        Map <String, Object> browserstackOptions = new HashMap<>();

        String myBrowser = null;
        String myBrowserVersion = null;
        if (browserType.equals(Config.prop.getProperty("CHROME"))) {
            myBrowser = "Chrome";
            myBrowserVersion = "62.0";
        }
        else if (browserType.equals(Config.prop.getProperty("FIREFOX"))) {
            myBrowser = "Firefox";
            myBrowserVersion = "67.0";
        }
        else if (browserType.equals(Config.prop.getProperty("IE"))) {
            myBrowser = "IE";
            myBrowserVersion = "11.0";
        }

        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("browser", myBrowser);
        caps.setCapability("browser_version", myBrowserVersion);
        caps.setCapability("browserstack.local", "true");
        caps.setCapability("browserstack.selenium_version", "3.141.59");
        browserstackOptions.put("userName", remoteDriverUserName);
        browserstackOptions.put("accessKey", remoteDriverPassword);
        caps.setCapability("bstack:options", browserstackOptions);

        return caps;
    }

    private void NullBrowserException(String browserType, Exception e){
        System.out.println("Can not find "+browserType+". Please make sure the correct browser has been selected. Exception: "+e);
    }

    //--------------------------------------------------------------------------------------//
    //-------------------------------------WINDOW HANDLES-----------------------------------//
    //--------------------------------------------------------------------------------------//

    private String lastWindowHandle = "";
    private void AssignDriversCurrentWindowToLastTab() {
        lastWindowHandle = webdriver.getDriver().getWindowHandle();
    }
    /**
     * Closes currently open window. Will close browser if only one window is open. Also attepts to switch to last used window.
     */
    protected void CloseCurrentBrowserWindow(){
        WebDriver driver = webdriver.getDriver();
        driver.close();
        try {
            driver.switchTo().window(lastWindowHandle);
        }
        catch (Exception e){
            System.out.println("Window "+lastWindowHandle+" either can not be found or does not exist. Exception: " + e);
        }
    }

    /**
     * Closes the selected window. If it is the only window. Closes the session.
     * @param windowName Name of window to close
     */
    protected void CloseBrowserWindow(String windowName){
        WebDriver driver = webdriver.getDriver();
        driver.switchTo().window(windowName).close();
        try {
            driver.switchTo().window(lastWindowHandle);
        }
        catch (Exception e){
            System.out.println("Window "+lastWindowHandle+" either can not be found or does not exist. Exception: " + e);
        }
    }
    /**
     * Attempts to switch to the last window the user was on.
     */
    protected void SwitchToLastBrowserWindow(){
        WebDriver driver = webdriver.getDriver();
        try {
            String lastTab = driver.getWindowHandle();
            driver.switchTo().window(lastWindowHandle);
            lastWindowHandle = lastTab;
        }
        catch(Exception e) {
            System.out.println("Window "+lastWindowHandle+" either can not be found or does not exist. Exception: " + e);
        }
    }

    /**
     * Switches to a window via it's name (not url).
     * @param windowName name of the window to switch to
     */
    protected void SwitchToWindow(String windowName)
    {
        AssignDriversCurrentWindowToLastTab();

        WebDriver driver = webdriver.getDriver();
        Set<String> windowHandles = driver.getWindowHandles();
        for (String window : windowHandles){
            driver.switchTo().window(window);
            if (driver.getPageSource().contains(windowName))
                break;
        }
    }
    /**
     * Sets the window to a specific pixel size
     * @param width width in pixels
     * @param height height in pixels
     */
    public void ChangeWindowSize(int width, int height){
        webdriver.getDriver().manage().window().setSize(new Dimension(width, height));
    }
    /**
     * Sets window to max size
     */
    public void MaximizeWindowSize(){
        webdriver.getDriver().manage().window().fullscreen();
    }
    /**
     * Closes down driver. Used at end of test
     */
    protected void DisposeDriver() {
        try {
            webdriver.getDriver().quit();
        }
        catch (NullPointerException e){}
    }

    //--------------------------------------------------------------------------------------//
    //------------------------------------File Searching------------------------------------//
    //--------------------------------------------------------------------------------------//

    private String driverLocation;

    /**
     * Fetches specified driver folder specified in config.properties.
     * On fail grabs from project directory
     * On fail. Exception
     * @param driverName Name of driver file. i.e. "chromedriver.exe"
     * @return returns driver executable
     */
    private String FetchDriver(String driverName) {
        //Grabs driver folder specified location specified
        try{
            return FetchFileFromDirectory(driverName, Config.prop.getProperty("DriverLocation"));
        }
        catch (IOException e){
            //Searches for driver from local directory
            try{
                return FetchFileFromDirectory(driverName, System.getProperty("user.dir"));
            }
            catch (IOException e1){
                TestAssertion.EndTests("Could not find usable driver.\n" +
                        " - Change the directory '"+ Config.prop.getProperty("DriverLocation")+"' to a valid directory\n" +
                        " - Place relevant drivers in the above directory\n" +
                        " You can also place drivers inside this project folder.\n" +
                        " Exception: "+e);

            }
        }
        return null;
    }

    private String FetchFileFromDirectory(String fileName, String directoryLocation) throws IOException{
        driverLocation = null;
        File directory = new File(directoryLocation);
        if (directory.isDirectory()) {
            search(directory, fileName);
        }
        else {
            throw new IOException(directory.getAbsoluteFile() + " is not a directory!");
        }
        if (driverLocation == null){
            throw new IOException();
        }
        return driverLocation;
    }

    private void search(File file, String fileName) {
        if (file.isDirectory()) {
            //do you have permission to read this directory?
            if (file.canRead()) {
                for (File subFile : file.listFiles()) {
                    if (subFile.isDirectory())
                        search(subFile, fileName);
                    else
                        if (fileName.toLowerCase().equals(subFile.getName().toLowerCase())) {
                            driverLocation = subFile.toString();
                            return;
                        }
                }
            }
        }
    }

    //--------------------------------------------------------------------------------------//
    //------------------------------------ALM CONNECTIONS-----------------------------------//
    //--------------------------------------------------------------------------------------//

    //Suite Setup
    private boolean usingAlm(){
        return this.getClass().getAnnotation(TestClass.class).usingAlm();
    }

    private void PostResults(ITestResult result, boolean usingAlm) throws IOException {
        if (usingAlm)
            //Use if test results & data are being uploaded to ALM
            PostMethodResultsToAlm(result);        //<--------Throws IOException
        else
            //Use if test results are not being uploaded to ALM
            WebSiteControl.saveTestScreenShots();  //<--------If using, comment CheckAlmMethodDataForErrors(m) in beforeMethod(m) above
    }

    private String methodName;
    private String testSetId;
    private String testId;

    /**
     * Checks method has correct data & skips test if it is not
     * @param m Method about to be called
     */
    private void CheckAlmMethodDataForErrors(Method m) {
        //Fetches Alm testSetId & testId
        methodName = m.getName();
        testSetId = GetTestSetId(methodName);
        testId = GetTestId(methodName);

        //Check if Method data is set correctly
        CheckForNonSetTestId(methodName, testSetId, testId);
    }

    /**
     * Posts all test results & attachments to ALM
     * @param result Results of test method just run by user.
     * @throws Exception Unable to establish a connection to ALM. Make sure config.properties is set
     */
    private void PostMethodResultsToAlm(ITestResult result) throws IOException{
        String s;

        //Saves screenshots as .zip and returns directory location
        String fileLocation = WebSiteControl.saveTestScreenShots();

        try {
            ALMConnection alm = new ALMConnection();
            ALMCaller almCaller = new ALMCaller(alm);

            testResult thisResult = CheckTestStatus(methodName, result.getStatus());

            String runId = null;

            if (thisResult == testResult.SUCCESS || thisResult == testResult.SUCCESS_PERCENTAGE_FAILURE)
                //ALM Call
                runId = almCaller.postTestResult(testSetId, testId, true, ScreenshotZip(fileLocation), "");
            else {                           //<------------thisResult == testResult.FAILURE
                //ALM Call
                runId = almCaller.postTestResult(testSetId, testId, false, ScreenshotZip(fileLocation), result.getThrowable().getMessage());
                //If user set defect to be posted
                if (postAsDefect) {
                    postAsDefect = false;
                    ChangeAlmResultToDefect(almCaller, runId);
                    defectSummary = null;
                }
            }
            return;
        }catch (Exception e){
            s = e.getMessage();
        }
        throw new SkipException("Error when talking to ALM within method '"+methodName+"'. Exception: " + s);
    }

    private static boolean postAsDefect = false;
    private static String testSeverity = null;
    private static String defectSummary = null;

    /**
     * Posts test as defect to ALM and fails it
     * @param i Fetches 'AlmTestSeverity' value written in config.properties. i.e. 1 is the first value, 5 is the fifth value
     * @param defectSummary User-made summary of defect
     */
    protected void PostAsDefectToAlm(int i, String defectSummary){
        WebSiteControl.TakeScreenshot();
        postAsDefect = true;
        this.defectSummary = defectSummary;
        BaseClass.testSeverity = ConvertTestSeverityToString(i);
        try{
            TestAssertion.EndTests(defectSummary);
        }
        catch (Exception e){}
    }
    /**
     * Posts test as defect to ALM
     * @param testSeverity Defect severity. Make sure this string matches a severity level in ALM or test will not be sent
     * @param defectSummary User-made summary of defect
     */
    protected void PostAsDefectToAlm(String testSeverity, String defectSummary){
        WebSiteControl.TakeScreenshot();
        postAsDefect = true;
        this.defectSummary = defectSummary;
        BaseClass.testSeverity = testSeverity;
        try{
            TestAssertion.EndTests(defectSummary);
        }
        catch (Exception e){}
    }

    private String ConvertTestSeverityToString(int i){
        return TestSeverity.getValue(i);
    }

    private void ChangeAlmResultToDefect(ALMCaller almCaller, String runId) throws Exception{
        almCaller.postDefect(runId,defectSummary,testSeverity);
    }

    private File ScreenshotZip(String zipFileLocation){
        if (zipFileLocation == null)
            return null;
        else
            return new File(zipFileLocation);
    }
    private void CheckForNonSetTestId(String methodName, String setId, String id) {
        if (setId.equals("0") || id.equals("0")){
            throw new SkipException("User has not set AlmMetaData for '"+methodName+"' correctly. " +
                    "Please update testSetId and testId.");
        }
    }
    private String GetTestSetId(String methodName) {
        return Integer.toString(AnnotationData(AlmMetaData.class,methodName).testSetId());
    }
    private String GetTestId(String methodName) {
        return Integer.toString(AnnotationData(AlmMetaData.class,methodName).testId());
    }

    private enum testResult{
        CREATED,
        SUCCESS,
        FAILURE,
        SKIP,
        SUCCESS_PERCENTAGE_FAILURE,
        STARTED
    }

    private testResult CheckTestStatus(String resultName, int result) {
//            int CREATED = -1;
//            int SUCCESS = 1;
//            int FAILURE = 2;
//            int SKIP = 3;
//            int SUCCESS_PERCENTAGE_FAILURE = 4;
//            int STARTED = 16;
        if (result == 1)
            return testResult.SUCCESS;
        else if (result == 2)
            return testResult.FAILURE;
        else if (result == -1)
            return testResult.CREATED;
        else if (result == 3)
            return testResult.SKIP;
        else if (result == 4)
            return testResult.SUCCESS_PERCENTAGE_FAILURE;
        else if (result == 16)
            return testResult.STARTED;
        else {
            System.out.println("Test '" + resultName + "' returned unexpected value. Please check test. Setting value to fail.");
            return testResult.FAILURE;
        }
    }

    private <T extends Annotation> T AnnotationData(Class<T> clazz, String methodName) {
        String s;
        Class<?> c = getClass();
        try {
            Method m = c.getMethod(methodName);
            final Annotation[] annos = m.getAnnotations();
            for (Annotation anno : annos) {
                if (clazz.isInstance(anno)) {
                    final T a = (T) anno;
                    return a;
                }
            }
            throw new SkipException("Can not find annotation AlmMetaData in method '" + m + "'.");
        }
        catch (Exception e){
            s = e.getMessage();
        }
        throw new SkipException(s);
    }
}
